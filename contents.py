import pathlib

def copy_files(folder):
    
    cwd = pathlib.Path(folder)
    contents = open('contents.txt', 'a')

    for file_or_dir in cwd.iterdir():

        if file_or_dir.is_file() and file_or_dir.name != 'contents.txt' and file_or_dir.name[-4:] == '.txt':
            with open(file_or_dir) as file:
                file_content = file_or_dir.name + ' : \n' + file.read() + "\n\n"
            contents.write(file_content)
        
        elif file_or_dir.is_dir():
            copy_files(file_or_dir)

    contents.close()        

wd = pathlib.Path('.')
copy_files(wd)
